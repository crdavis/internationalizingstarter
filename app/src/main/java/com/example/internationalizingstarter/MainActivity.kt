package com.example.internationalizingstarter

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.internationalizingstarter.ui.theme.InternationalizingStarterTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            InternationalizingStarterTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    IntentDemo()
                }
            }
        }
    }
}

@SuppressLint("QueryPermissionsNeeded")
@Composable
fun IntentDemo ( context: Context = LocalContext.current) {

    Column (
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Button(onClick = {
            val intent = Intent(context, AnotherActivity::class.java)
            context.startActivity(intent)
        } ) {
            Text("Start Activity")
        }

        Spacer(modifier = Modifier.height(40.dp))

        Button(onClick = {
            Intent(Intent.ACTION_MAIN).also { it.`package`="com.google.android.youtube"
                try {
                    context.startActivity(it)
                } catch (e:ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
        }) {
            Text("Start Youtube")
        }

        Spacer(modifier = Modifier.height(40.dp))

        Button(onClick = {
            val intent = Intent(Intent.ACTION_SEND).apply {
                type = "text/plain"
                putExtra(Intent.EXTRA_EMAIL, arrayListOf("yourAddress@gmail.com"))
                putExtra(Intent.EXTRA_SUBJECT, "Subject of the email")
                putExtra(Intent.EXTRA_TEXT, "This is the body of the email")
            }
            if(intent.resolveActivity(context.packageManager) != null) {
                context.startActivity(intent)
            }
        }) {
            Text(text = "Send Email")
        }

        Spacer(modifier = Modifier.height(40.dp))

        OutlinedButton(onClick = {
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://gitlab.com/crdavis/intentsexamplecode"))
            context.startActivity(webIntent)
        }) {
            Text(text = "Open Website")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    InternationalizingStarterTheme {
        IntentDemo()
    }
}